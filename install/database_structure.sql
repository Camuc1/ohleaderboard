CREATE TABLE `leaderboard` (
`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
`username` varchar(255) NOT NULL,
`score` int(11) NOT NULL,
`score_updates` int(11) NOT NULL
) DEFAULT CHARSET=utf8;