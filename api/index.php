<?php

	header('Cache-Control: no-cache, must-revalidate');
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	header('Content-type: application/json; charset="UTF-8"');	

	require_once("commons/commons.php");

	$ohapi=new OHAPIcontroller($_SERVER['REQUEST_METHOD']);
	echo json_encode($ohapi->respond(),JSON_PRETTY_PRINT);

	
	exit();