<?php

    class OHLeaderboard {
        
        private $db;

        function __construct(){
            $this->db=new OHDB();
            $this->db->OHconnect();
        }

        function findEntryByName($name){
            $res=$this->db->query("SELECT * FROM `leaderboard` WHERE `username`='".$this->db->escape_string($name)."' LIMIT 1");
            if($res && $entry=$res->fetch_assoc()) {
                return([
                        "success" => true,
                        "data" => $row,
                        "message" => "Entry found"
                ]);
            } else {
                    return([
                        "success" => false,
                        "name" => $name,
                        "message" => "Entry not found"
                ]);
            }
        }

        function createEntry($name,$score){
            $result=$this->findEntryByName($name);
            if(trim($name)) {
                if($result["success"]===false) {
                    $this->db->query("INSERT INTO `leaderboard` SET
                        `id`=NULL,
                        `username`='".$this->db->escape_string($name)."',
                        `score`=".$this->db->escape_string($score*1).",
                        `score_updates`=0;
                    ");
                    if($this->db->insert_id>0) {
                        return([
                            "success" => true,
                            "id" => $this->db->insert_id,
                            "message" => $name." added to leaderboard"
                        ]);
                    } else {
                        return([
                            "success" => false,
                            "name" => $name,
                            "message" => "DB Error"
                        ]);                
                    }
                } else {
                    return([
                        "success" => false,
                        "name" => $name,
                        "message" => "User already present"
                    ]);                
                }
            } else {
                return([
                    "success" => false,
                    "name" => $name,
                    "message" => "Invalid username (empty)"
                ]);                
            }
        }

        function updateEntry($name,$score){
            $result=$this->findEntryByName($name);
            if($result["success"]===true) {
                $this->db->query("UPDATE `leaderboard` SET
                    `score`=".$this->db->escape_string($score*1)."
                    WHERE `username`='".$this->db->escape_string($name)."' AND `score`<".$this->db->escape_string($score*1)."
                    LIMIT 1
                ");
                $this->db->query("UPDATE `leaderboard` SET
                    `score_updates`=`score_updates`+1 
                    WHERE `username`='".$this->db->escape_string($name)."'
                    LIMIT 1
                ");
                if($this->db->affected_rows>0) {
                    return([
                        "success" => true,
                        "id" => $name,
                        "message" => $name." updated in leaderboard",
                    ]);
                } else {
                    return([
                        "success" => false,
                        "name" => $name,
                        "message" => "DB Error"
                    ]);                
                }
            } else {
                return([
                    "success" => false,
                    "name" => $name,
                    "message" => "User not found"
                ]);                
            }
        }
        
        function deleteEntry($name){
            $result=$this->findEntryByName($name);
            if($result["success"]===true) {
                $this->db->query("DELETE FROM `leaderboard` WHERE `username`='".$this->db->escape_string($name)."' LIMIT 1");
                if($this->db->affected_rows>0) {
                    return([
                        "success" => true,
                        "name" => $name,
                        "message" => $name." deleted from leaderboard"
                    ]);
                } else {
                    return([
                        "success" => false,
                        "name" => $name,
                        "message" => "DB Error"
                    ]);                
                }
            } else {
                return([
                    "success" => false,
                    "name" => $name,
                    "message" => "User not found"
                ]);                
            }
        }

        function getEntries($startrank,$quantity){

            $res=$this->db->query("SELECT ranks . * 
                FROM ( 
                    SELECT RANK() OVER (ORDER BY score DESC) AS rankposition, score, username, score_updates, id 
                    FROM `leaderboard` 
                    ORDER BY score DESC) ranks 
                WHERE rankposition>=".($startrank*1)." 
                limit ".($quantity*1));

            $entries=[];
            while($res && $entry=$res->fetch_assoc()){
                array_push($entries,$entry);
            }
            return([
                "success" => true,
                "data" => $entries,
                "message" => count($entries)." entries found"
            ]);
        }

        function countEntries(){
            $res=$this->db->query("SELECT count(*) as cnt from `leaderboard`");
            if($res && $cnt=$res->fetch_assoc()){
                return([
                    "success" => true,
                    "data" => $cnt["cnt"],
                    "message" => $cnt["cnt"]." entries in the leaderboard"
                ]);
            }
            return([
                "success" => false,
                "name" => $name,
                "message" => "DB Error"
            ]);
        }

        function averageScore(){
            $res=$this->db->query("SELECT avg(score) as average from `leaderboard`");
            if($res && $avg=$res->fetch_assoc()){
                return([
                    "success" => true,
                    "data" => $avg["average"],
                    "message" => $avg["average"]." is the average score"
                ]);
            }
            return([
                "success" => false,
                "name" => $name,
                "message" => "DB Error"
            ]);
        }

    }