<?php

	class OHDB extends mysqli {

		public function OHconnect() {
			require_once(__DIR__."/../settings/db.php");
			if(isset($dbHost) && isset($dbUser) && isset($dbPassword) && isset($dbName)) {
				$ret=$this->connect($dbHost,$dbUser,$dbPassword,$dbName);
				if(false===$ret) {
					echo ($dbHost).($dbUser).($dbPassword).($dbName);
					throw new Exception("DB not connected");
				}
			}
			else {
				echo ($dbHost).($dbUser).($dbPassword).($dbName);
				throw new Exception("DB login not set");
			}
		}
	}