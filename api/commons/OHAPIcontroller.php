<?php

    class OHAPIcontroller {

        private $params=false;
        private $edges=false;
        private $HTTPmethod=false;

        function __construct($HTTPmethod) {
            $this->HTTPmethod=$HTTPmethod;
            if($HTTPmethod=="GET") {
                $this->params=$_GET;
            }
            else {
                //parse_str(file_get_contents("php://input"),$this->params);
                $jsonContent=file_get_contents("php://input");
                $this->params=json_decode($jsonContent,true);
            }
            $this->edges=explode("/",$_GET["edges"]);
        }
        
        /**
         * uses http method and the given function name to perform the correct operation
         *
         * @return output or throws exception
         */
        function respond() {
            $endpoint=$this->edges[0];
            $methodName="_".strtolower($this->HTTPmethod)."_".$endpoint;
            if(method_exists($this,$methodName)) {
                return $this->$methodName();
            } else {
                throw new Exception("Endpoint not managed");
            }
        }

        private function _get_topentries() {
            $lb=new OHLeaderboard();
            $quantity=$this->edges[1]*1;
            return $lb->getEntries(1,$quantity);
        }

        private function _post_entry() {
            $lb=new OHLeaderboard();
            $name=$this->params["name"];
            $score=$this->params["score"];
            return $lb->createEntry($name,$score);
        }

        private function _put_entry() {
            $lb=new OHLeaderboard();
            $name=$this->params["name"];
            $score=$this->params["score"];
            return $lb->updateEntry($name,$score);
        }

        private function _delete_entry() {
            $lb=new OHLeaderboard();
            $name=$this->params["name"];
            return $lb->deleteEntry($name,$score);
        }

        private function _get_average() {
            $lb=new OHLeaderboard();
            return $lb->averageScore();
        }

        private function _get_count() {
            $lb=new OHLeaderboard();
            return $lb->countEntries();
        }


    }