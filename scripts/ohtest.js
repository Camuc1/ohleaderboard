function ajaxCall(method, endpoint, data, callback) {
    xhr = new XMLHttpRequest();
    xhr.open(method, '/api/'+endpoint);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (xhr.status === 200) {
            callback(JSON.parse(xhr.responseText));
        }
        else if (xhr.status !== 200) {
            if(NULL!=xhr.responseText) {
                alert('Request failed.  Returned status of ' + xhr.status);
                }
            } else {
                errobj=JSON.parse(xhr.responseText);
                alert(errobj["message"]);
        }
    };
    xhr.send(JSON.stringify(data));
}

function leaderboardMessage(dataReceived) {
    alert(dataReceived.message);
    readLeaderboard();
}

function newEntry(name,score) {
    ajaxCall('POST','entry',{name: name,score: score},leaderboardMessage);
}
function modEntry(name,score) {
    ajaxCall('PUT','entry',{name: name,score: score},leaderboardMessage);
}
function deleteEntry(name) {
    ajaxCall('DELETE','entry',{name: name},leaderboardMessage);
}

function doModEntry(id) {
    var name=document.getElementById('uname'+id).innerHTML;
    var score=document.getElementById('uscore'+id).value;
    modEntry(name,score);
}

function doNewEntry(id) {
    var name=document.getElementById('newusername').value;
    var score=document.getElementById('newscore').value;
    newEntry(name,score);
}

function doDeleteEntry(id) {
    var name=document.getElementById('uname'+id).innerHTML;
    deleteEntry(name);
}

function getAverage() {
    ajaxCall('GET','average',{},leaderboardMessage);
}

function getCount() {
    ajaxCall('GET','count',{},leaderboardMessage);
}

function readLeaderboard() {
    ajaxCall('GET','topentries/'+document.getElementById('usersqt').value,{},buildLeaderboard);        
}

function buildLeaderboard(dataReceived) {
    var lbContainer=document.getElementById('leaderboardContainer');
    lbContainer.innerHTML='';
    for(ind in dataReceived.data) {
        entry=dataReceived.data[ind];
        var entrydiv = document.createElement("div");
        entrydiv.setAttribute('id','lbe'+entry.id);
        entrydiv.setAttribute('class','lbeline');        
        entrydiv.setAttribute('entryname',entry.username);
        entrydiv.innerHTML=(
            '<div class="urank">Rank '+entry.rankposition+' ('+entry.score_updates+' upd)</div>'+
            '<div class="uname" id="uname'+entry.id+'">'+entry.username+'</div>'+
            '<div class="uupdates">'+entry.score_updates+' upd</div>'+
            '<input class="uscore" type="text" id="uscore'+entry.id+'" value="'+entry.score+'" />'+
            '<input class="modbtn" type="button" onclick="doModEntry('+entry.id+');" value="UPDATE" />'+
            '<input class="delbtn" type="button" onclick="doDeleteEntry('+entry.id+');" value="DELETE" />');
        lbContainer.appendChild(entrydiv);
    }
}